# SuperMUC setup

## Firewall
First change your passwords at https://idportal.lrz.de/r/entry.pl?Sprache=en .

The **local** IP address of your computer needs to be added to the IP table of the SuperMUC firewall at https://www.lrz.de/services/compute/supermuc/projectproposal/

To get your local IP address use www.whatismyip.com

## SSH keys
Add the following to your list of known hosts in `~/.ssh/known_hosts`
```bash
# Keys for supermuc phase 1 fat login nodes (login01,login02)
wm.supermuc.lrz.de,supermuc-fat.lrz.de ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJ/rNKoq7mYM1XlxDP4o7ouEtz+YvHbLRKPZi7WjrDS7oDWsVxSYoPtbuw0OcNhZeGUuatRI4m6QHJ8t5ZOk9FY=
# Keys for supermuc phase 1 thin login nodes (login03,login04,login05,login06,login07
sb.supermuc.lrz.de,supermuc.lrz.de ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJ/rNKoq7mYM1XlxDP4o7ouEtz+YvHbLRKPZi7WjrDS7oDWsVxSYoPtbuw0OcNhZeGUuatRI4m6QHJ8t5ZOk9FY=
# Keys for phase 1 TSM archive nodes
sb-tsm.supermuc.lrz.de,supermuc-tsm.lrz.de ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJ/rNKoq7mYM1XlxDP4o7ouEtz+YvHbLRKPZi7WjrDS7oDWsVxSYoPtbuw0OcNhZeGUuatRI4m6QHJ8t5ZOk9FY=
# Keys for phase 2 login nodes (login21,login22,login23)
hw.supermuc.lrz.de ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEoQbvPzImTvlyO9NhD40Js3hYebg+YWyPQ4X0Okfi9Se5r4slYys6R7MhTPwK9JXg6kQqkjgFiYTvYy96P0JlM=
# Keys for phase 2 TSM archive nodes
hw-tsm.supermuc.lrz.de ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEoQbvPzImTvlyO9NhD40Js3hYebg+YWyPQ4X0Okfi9Se5r4slYys6R7MhTPwK9JXg6kQqkjgFiYTvYy96P0JlM=
```
If you don't already have a pair of SSH keys then make them on your personal deskop.
```bash
ssh-keygen -t rsa
```
Create the SSH settings on SuperMUC:
```bash
mkdir -p ~/.ssh
chgrp $(id -g -n) ~/.ssh
chmod o-rwx,g-w ~
chmod go-rwx ~/.ssh
```
Add the public SSH key from your local `~/.ssh/id_rsa.pub` to the SumperMUC `~/.ssh/authorized_keys`
Add the Following settings to your local `~/.ssh/config`. My username for SuperMUC-NG ends with a 2. The older SuperMUC ends with 3.
```bash
Host skx.supermuc
    HostName skx.supermuc.lrz.de
    User <username>
    IdentityFile ~/.ssh/id_rsa
```
Then connect with:
```bash
ssh skx.supermuc
```

## MPI SVN server access
If you want to access the MPI SVN server, you need to port forward the SVN repository. In your local `~/.ssh/config` file, add the following `LocalForward` entry, but replace `<NNNNN>` with an arbitrary port number.
```bash
Host skx.supermuc
    HostName skx.supermuc.lrz.de
    User <username>
    IdentityFile ~/.ssh/supermuc_key
    LocalForward <NNNNN> svn.zmaw.de:443
```
Then in SuperMUC you can access SVN repositories like so:
```bash
svn list https://<YOURSVNUSERNAME>@localhost:<NNNNN>/svn/cosmos/branches
```

# Getting the repository
If you check out a repository directly from the MPI SVN server on to SuperMUC, there are likely missing libraries. YAXT and CDI are included in the source code of MPI-ESM, but due to security measures they will not be automatically checked out.
You must check out locally and then manually transfer the CDI library to ECHAM source directory using `scp`.
```bash
svn checkout https://svn.zmaw.de/svn/cosmos/branches/mpiesm-landveg-cmip6
```

## Patch for SuperMUC

You also need to apply the patch to the repository to modify the configuration and compilation scripts so that they work on SuperMUC. This can be done before it is transferred to SuperMUC.

```bash
git clone https://gitlab.com/tammasloughran/supermuc.patch.git
cp ./supermuc.patch/supermuc.patch ~/mpiesm-landveg-cmip6
cd ~/mpiesm-landveg-cmip6
patch -p0 -i supermuc.patch
scp -r ./piesm-landveg-cmip6 skx.supermuc:~
```

# Module load changes
The MPI library supported is Intel MPI. The corresponding module is called mpi.intel.
It can be handy to add the following modules to your `$HOME/.bashrc` file modules:
```bash
module load hdf5 netcdf netcdf-fortran cdo szip mpi.intel
```

# Create experiments
You can follow the instructions in the RECIPE files in the root of the repository to create experiments. Both the old IMDI and mkexp scripting environments should work. If you choose to use mkexp please referr to the section other dependancies for installing dependancies related to the monitoring plotting routines.

## mkexp
Create a copy of one of the example configuration files eg. `exptest.config`. Edit your configuration file:
```
ACCOUNT = pn72wo
EMAIL=<YOUREMAIL>

EXP_TYPE = jsb_piControl-LR
MODEL_VERS = I01
MODEL_SUBDIR=mpiesm-landveg-cmip6
```
Run the following command to make experiments for JSBALONE:
```bash
../util/mkexp/mkexp -p ../run_jsbach:. exptest.config ENVIRONMENT=supermuc
```


# Other dependancies

`mkexp` requires python ImageMagick and NCL to run the plotting scripts smoothly.

## ImageMagick
It is not available on SuperMUC at the moment but a request for it has been made. There are prebuilt binaries available on their website but I opted for compiling it myself.

Locally:
```bash
wget https://imagemagick.org/download/ImageMagick.tar.gz
scp ImageMagick.tar.gz skx.supermuc:~
```
Then remotely:
```bash
tar -vxzf ImageMagick.tar.gz
cd ImageMagick-7.0.9-8
./configure --prefix=/dss/dsshome1/0D/ru87jek2
make
make install
```

## Python and NCL

The default python module on SuperMUC is very bare-bones. Because only ssh connections are allowed, its difficult to install new python packages. It's easy enough to install on your own computer then transfer to SuperMUC.
```bash
curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh > Miniconda3-latest-Linux-x86_64.sh
sudo bash Miniconda3-latest-Linux-x86_64.sh
```
Install to the same directory that it would exist in on SuperMUC. e.g. `/dss/dsshome1/0D/<YOURUSER>/miniconda3` . Say no to initialisation. Install all the packages you could possibly need. `tar` the installation and `scp` to SuperMUC.
NCL is also not available on SuperMUC. NCL can be installed with anaconda.

```bash
source /dss/dsshome1/0D/<YOURUSER>/miniconda3/etc/profile.d/conda.sh
conda intall -c conda-forge ncl
tar -vczf miniconda3.tar.gz /dss/dsshome1/0D/<YOURUSER>/miniconda3
scp miniconda3.tar.gz skx.supermuc.lrz.de:~
```
Then on SuperMUC:
```bash
tar -vxzf miniconda3.tar.gz
eval "$(/dss/dsshome1/0D/<YOURUSER>/miniconda3/bin/conda shell.bash hook)"
conda init
```

The configuration files have already been modified to activate my own conda environment and run ncl in bash. You may want to change them to use your own if you like. The monitoring script also requires the nclsh wrapper to be in a PATH directory. Get it from
https://www.ncl.ucar.edu/Support/talk_archives/2012/att-1197/nclsh
put it in your `$HOME/bin` directory

`mkexp` Requires python to generate the scripts. The anaconda distribution that is already available on SuperMUC works well for this.
```bash
module unload python
module load python/2.7_anaconda_nompi
```

# Common problems

## Pool data
All pool data that are required to run the default piControl-LR configurations are located in `/hppfs/work/pn72wo/ru87jek2/pool/data/`
If the data are not found at runtime it is likely that the configuration requires data that are missing. Download from mistral and place in the equivalent directory.

## C/N ratio mismatch
There are usually some C/N ratio mismatch warnings on the CMIP6 version of the model. This is because there was a recent change in the C/N ratio within the model and the old initialisation files for nitrogen were created with old spinup simulations. To get rid of them you need to spin up from scratch of initialise nitrogen pools from the carbon pools.
