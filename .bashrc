# Commonly used modules 
module load cdo
module load nco
module load ncview/2.1.7-impi

# Work directory
export WORK=/hppfs/work/pn72wo/ru87jek2

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/dss/dsshome1/0D/ru87jek2/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/dss/dsshome1/0D/ru87jek2/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/dss/dsshome1/0D/ru87jek2/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/dss/dsshome1/0D/ru87jek2/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# Add the home directory to library path
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}:${HOME}/lib
export LD_RUN_PATH=${HOME}

# Define default compiler for MPI-ESM
export CC=mpiicc
export FC=mpiifort

# MPI-ESM compiling modules (redundant, these are now in the landveg and config files)
module load hdf5 netcdf netcdf-fortran cdo szip mpi.intel
export NETCDFFROOT=${NETCDF_BASE}
export NETCDFROOT=${NETCDF_BASE}
export SZIPROOT=${SZIP_BASE}


# Define a function to convert grb files to netcdf.
grb2nc(){
    cdo -f nc -r -t $2 copy $1 ${1::-3}nc
}


# Aliases
alias queue='squeue -u ru87jek2'
